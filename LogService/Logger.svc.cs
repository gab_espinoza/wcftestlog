﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using NLog;
using NLog.LogReceiverService;

namespace LogService
{

    public class Logger : ILogReceiverServer
    {
        public void ProcessLogMessages(NLogEvents events)
        {
            foreach (var eachEvent in events.ToEventInfo())
            {
                var logger = LogManager.GetLogger(eachEvent.LoggerName);
                logger.Log(eachEvent);
            }
        }
    }
}
